require 'sinatra'
require 'slim'
require 'less'

set :views, :less => 'assets/stylesheets', :default => 'views'

helpers do
  def find_template(views, name, engine, &block)
    _, folder = views.detect { |k,v| engine == Tilt[k] }
    folder ||= views[:default]
    super(folder, name, engine, &block)
  end
end

get '/' do
  slim :index
end

get '/:style.css' do
  less params[:style].to_sym
end

get '/:image.png' do
  send_file File.join(settings.root, "assets/images/#{params[:image]}.png")
end